# Create class based Python script that reads through order_listing files in /raw_date and forms a single .csv file as an output
# (no need to check for duplicate rows)
# There are also other files in /raw_data and those should be ignored.

# import OS module
from asyncore import read
from dataclasses import replace
from fileinput import filename
from itertools import count
from operator import xor
import os
from ossaudiodev import control_labels
import re
import csv
import datetime

class Sort:
    def __init__(self):
        pass

    def seperate_files(self):
        filenames = []

        # Gets the list of all files and directories
        # Checks for full path
        path = "raw_data/"
        dir_list = os.listdir(path)

        # Seperates order_listing files from all other files
        # The .+ symbol is  used to match one or more characters
        for word in dir_list:
            if re.search('order_listing_.+', word) :
                print(word)
                filenames.append(word)
        return filenames

    #read()-function reads through given csv files data and skippes firs line (headers)
    def read(self, filename, datalines):
        with open(filename, encoding="utf-8") as csvfile_read:
            counter = 0
            for line in csvfile_read:
                #skip header
                if counter == 0:  
                    counter+=1
                    continue
                #print(line)
                datalines.append(line)

        csvfile_read.close()
        return datalines
    
    # write()-function writes data to a new .csv file: listed data which includes name;age_group;order_date;total
    # filename contains "Pre_Delivery_" + date when material was formed
    def write(self, datalines):
        x = datetime.datetime.now()
         
        with open("pre_delivery/Pre_Delivery_" + x.strftime("%Y%m%d") +".csv","a", encoding="utf-8",errors="replace") as csvfile_write:
            column_names = ["name","age_group","order_date","total"]
            csv_writer = csv.DictWriter(csvfile_write, fieldnames=column_names, delimiter=";")
            csv_writer.writeheader()

            for line in datalines:
                (var1,var2,var3,var4,var5) = line.split(";")

                l=[[0,18],[19,20],[21,30],[31,40],[41,50],[51,60],[61,70],[71,80],[81,90],[91]] #age groups
                print(var2)

                #categorize ages to age groups
                for x in l:
                    if int(var2) > (l[-1])[0]:
                        var2 = str((l[-1])[0]) + "+"
                        print(var2)
                        break
                    elif int(var2) > x[-1]:
                        continue
                    var2 = str(x[0]) + "-" + str(x[-1])
                    print(var2)
                    break

                #choose rows where var5 (=delivered) = False
                r_filter ="\n"
                filtered_var5 = re.sub(r_filter, '',var5)
                if filtered_var5=="False":
                    csv_writer.writerow({
                        "name": var1,
                        "age_group": var2,
                        "order_date": var3,
                        "total": var4,
                        })

        csvfile_write.close()

    def run(self):
        
        filenames = self.seperate_files()

        datalines=[]

        for filename in filenames:
            datalines = self.read("raw_data/"+filename, datalines)
            #print(datalines)

        self.write(datalines)

if __name__ == "__main__":
    sort = Sort()
    sort.run()



